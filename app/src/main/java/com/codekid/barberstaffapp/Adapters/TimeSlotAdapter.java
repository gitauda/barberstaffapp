package com.codekid.barberstaffapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codekid.barberstaffapp.AfterServiceActivity;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Interfaces.IItemSelectedListener;
import com.codekid.barberstaffapp.Models.BookingInformation;
import com.codekid.barberstaffapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.TimeSlotViewHolder> {

    Context context;
    List<BookingInformation> timeSlots;
    List<CardView> cardViewList;


    public TimeSlotAdapter(Context context) {
        this.context = context;
        this.timeSlots = new ArrayList<>();
        cardViewList = new ArrayList<>();
    }

    public TimeSlotAdapter(Context context, List<BookingInformation> timeSlots) {
        this.context = context;
        this.timeSlots = timeSlots;
        cardViewList = new ArrayList<>();
    }

    @NonNull
    @Override
    public TimeSlotViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_time_slot, viewGroup, false);
        return new TimeSlotViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TimeSlotViewHolder holder, int position) {
        holder.txt_time.setText(new StringBuilder(Common.convertTimeSlotToString(position)).toString());
        if (timeSlots.size() == 0) // if all positions are available just show list
        {
            holder.txt_time_desc.setText(new StringBuilder("Available"));
            holder.txt_time_desc.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.txt_time.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.time_card.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));

            // holder.time_card.setEnabled(true);

            //Empty event
            holder.setiItemSelectedListener((view, position13) -> {
                //fixes crash
            });

        } else // if time slot already picked
        {
            for (BookingInformation slotVal : timeSlots) {
                //Loop all time slots from server and set different color
                int slot = Integer.parseInt(slotVal.getSlot().toString());
                if (slot == position) // if slot == position
                {

                    if (!slotVal.isDone()) {
                        //set a tag for all time slots that are full
                        //based on the tag we can set all remaining card backgrounds without changing the full time slots]
                        holder.time_card.setTag(Common.DISABLE_TAG);

                        holder.time_card.setCardBackgroundColor(context.getResources().getColor(android.R.color.darker_gray));

                        holder.txt_time_desc.setText(new StringBuilder("FULL"));
                        holder.txt_time_desc.setTextColor(context.getResources().getColor(android.R.color.white));
                        holder.txt_time.setTextColor(context.getResources().getColor(android.R.color.white));
                        // holder.time_card.setEnabled(true);

                        holder.setiItemSelectedListener((view, position12) -> {
                            //for gray time slots
                            //get booking info and store in common.currentBookingInfo
                            //after that, start AfterServicesActivity

                            FirebaseFirestore.getInstance()
                                    .collection("BarberShops")
                                    .document(Common.city_name)
                                    .collection("Branch")
                                    .document(Common.selectedShop.getShopId())
                                    .collection("Barbers")
                                    .document(Common.currentBarber.getBarberId())
                                    .collection(Common.dateFormat.format(Common.bookingDate.getTime()))
                                    .document(slotVal.getSlot().toString())
                                    .get()
                                    .addOnFailureListener(e -> Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show())
                                    .addOnCompleteListener(task -> {
                                        if (task.isSuccessful()) {
                                            if (task.getResult().exists()) {
                                                Common.currentBookingInfo = task.getResult().toObject(BookingInformation.class);
                                                Common.currentBookingInfo.setBookingId(task.getResult().getId());
                                                context.startActivity(new Intent(context, AfterServiceActivity.class));
                                            }
                                        }
                                    });

                            // Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();
                        });
                    }
                    else{
                        // if service is done
                        holder.time_card.setTag(Common.DISABLE_TAG);

                        holder.time_card.setCardBackgroundColor(context.getResources().getColor(android.R.color.holo_orange_dark));

                        holder.txt_time_desc.setText(new StringBuilder("DONE"));
                        holder.txt_time_desc.setTextColor(context.getResources().getColor(android.R.color.white));
                        holder.txt_time.setTextColor(context.getResources().getColor(android.R.color.white));

                        holder.setiItemSelectedListener((view, position14) -> {
                            //fixes crash
                        });
                    }
                }
                else
                {
                    if (holder.getiItemSelectedListener() == null){
                        //Event for view holder which doesn't implement click
                        //without this condition everytime a slot with higher current time will override the event
                        holder.setiItemSelectedListener((view, position1) -> {

                        });
                    }

                }
            }
        }

        //Add all time slots to the list (20 cards)
        //don't add card if it's already in the lsit
        if (!cardViewList.contains(holder.time_card)){

            cardViewList.add(holder.time_card);
        }



    }

    @Override
    public int getItemCount() {
        return Common.TIME_SLOT_TOTAL;
    }

    public class TimeSlotViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_time, txt_time_desc;
        CardView time_card;

        IItemSelectedListener iItemSelectedListener;

        public IItemSelectedListener getiItemSelectedListener() {
            return iItemSelectedListener;
        }

        public void setiItemSelectedListener(IItemSelectedListener iItemSelectedListener) {
            this.iItemSelectedListener = iItemSelectedListener;
        }

        public TimeSlotViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_time = itemView.findViewById(R.id.txt_time_slot);
            txt_time_desc = itemView.findViewById(R.id.time_slot_description);
            time_card = itemView.findViewById(R.id.time_slot_card);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            iItemSelectedListener.onItemSelectedListener(v, getAdapterPosition());
        }
    }
}
