package com.codekid.barberstaffapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codekid.barberstaffapp.Models.ShoppingItem;
import com.codekid.barberstaffapp.R;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfirmShoppingAdapter extends RecyclerView.Adapter<ConfirmShoppingAdapter.ConfirmViewHolder> {


    Context context;
    List<ShoppingItem> shoppingItems;

    public ConfirmShoppingAdapter(Context context, List<ShoppingItem> shoppingItems) {
        this.context = context;
        this.shoppingItems = shoppingItems;
    }

    @NonNull
    @Override
    public ConfirmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_confirm_shopping,parent,false);
        return new ConfirmViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmViewHolder holder, int position) {
        Picasso.get()
                .load(shoppingItems.get(position).getImage())
                .into(holder.item_image);
        holder.txt_name.setText(shoppingItems.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return shoppingItems.size();
    }

    public class ConfirmViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_image)
        ImageView item_image;
        @BindView(R.id.txt_name)
        TextView txt_name;

        public ConfirmViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
