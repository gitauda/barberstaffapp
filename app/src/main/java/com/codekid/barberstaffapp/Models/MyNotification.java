package com.codekid.barberstaffapp.Models;

import com.google.firebase.Timestamp;

public class MyNotification {
    private String uid,content,title;
    private boolean read;
    private Timestamp serverTimeStamp;

    public MyNotification() {
    }


    public MyNotification(String uid, String content, String title, boolean read, Timestamp serverTimeStamp) {
        this.uid = uid;
        this.content = content;
        this.title = title;
        this.read = read;
        this.serverTimeStamp = serverTimeStamp;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Timestamp getServerTimeStamp() {
        return serverTimeStamp;
    }

    public void setServerTimeStamp(Timestamp serverTimeStamp) {
        this.serverTimeStamp = serverTimeStamp;
    }
}
