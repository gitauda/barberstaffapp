package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.City;

import java.util.List;

public interface ICityLoadListener {
    void onAllBranchLoadSuccess(List<City> cityList);
    void onAllBranchLoadFailed(String message);
}
