package com.codekid.barberstaffapp.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


import com.codekid.barberstaffapp.BarberShopListActivity;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Interfaces.IItemSelectedListener;
import com.codekid.barberstaffapp.Models.City;
import com.codekid.barberstaffapp.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.BranchViewHolder> {

    Context context;
    List<City> cityList;

    int lastPosition=-1;

    public BranchAdapter(Context context, List<City> cityList) {
        this.context = context;
        this.cityList = cityList;
    }

    @NonNull
    @Override
    public BranchViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_cities,viewGroup,false);
        return new BranchViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BranchViewHolder holder, int position) {
        holder.txt_city_name.setText(cityList.get(position).getName());

        setAnimation(holder.itemView,position);

        holder.setiItemClickListener((view, position1) -> {
            Common.city_name = cityList.get(position1).getName();
            context.startActivity(new Intent(context, BarberShopListActivity.class));
        });
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context,
                    android.R.anim.slide_in_left);
            itemView.startAnimation(animation);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return cityList.size();
    }

    public class BranchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.txt_city_name)
        TextView txt_city_name;

        IItemSelectedListener iItemClickListener;

        public void setiItemClickListener(IItemSelectedListener iItemClickListener) {
            this.iItemClickListener = iItemClickListener;
        }

        public BranchViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            iItemClickListener.onItemSelectedListener(v,getAdapterPosition());
        }
    }
}
