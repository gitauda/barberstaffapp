package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.BarberServices;

import java.util.List;

public interface IBarberServicesLoadListener {
    void onBarberServicesLoadSuccess(List<BarberServices> servicesList);
    void onBarberServicesLoadFailed(String message);
}
