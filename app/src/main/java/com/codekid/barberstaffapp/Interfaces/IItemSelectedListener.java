package com.codekid.barberstaffapp.Interfaces;

import android.view.View;

public interface IItemSelectedListener {
    void onItemSelectedListener(View view, int position);
}
