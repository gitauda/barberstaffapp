package com.codekid.barberstaffapp.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Common.CustomLoginDialog;
import com.codekid.barberstaffapp.HomeActivity;
import com.codekid.barberstaffapp.Interfaces.IDialogClickListener;
import com.codekid.barberstaffapp.Interfaces.IGetBarberListener;
import com.codekid.barberstaffapp.Interfaces.IItemSelectedListener;
import com.codekid.barberstaffapp.Interfaces.UserLoginRememberListener;
import com.codekid.barberstaffapp.Models.Barber;
import com.codekid.barberstaffapp.Models.BarberShops;
import com.codekid.barberstaffapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

public class ShopAdapter  extends RecyclerView.Adapter<ShopAdapter.BarbershopViewHolder> implements IDialogClickListener {

    private Context mContext;
    private List<BarberShops> shopsList;
    private List<CardView> cardViewList;

    private UserLoginRememberListener rememberListener;
    private   IGetBarberListener barberListener;


    public ShopAdapter(Context mContext, List<BarberShops> shopsList, IGetBarberListener barberListener, UserLoginRememberListener rememberListener) {
        this.mContext = mContext;
        this.shopsList = shopsList;
        cardViewList = new ArrayList<>();
        this.barberListener = barberListener;
        this.rememberListener = rememberListener;
    }

    @NonNull
    @Override
    public BarbershopViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_shop,viewGroup,false);
        return new BarbershopViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final BarbershopViewHolder holder, int i) {
        holder.txt_shop_name.setText(shopsList.get(i).getName());
        holder.txt_shop_address.setText(shopsList.get(i).getAddress());
        if (!cardViewList.contains(holder.barber_card)){

            cardViewList.add(holder.barber_card);
        }

        holder.setiItemSelectedListener((view, position) -> {
            Common.selectedShop = shopsList.get(position);
            showLoginDialog();
        });
    }

    private void showLoginDialog() {
        CustomLoginDialog.getInstance()
                .showLoginDialog("STAFF LOGIN",
                        "LOGIN",
                        "CANCEL",
                        mContext,
                        this);
    }

    @Override
    public int getItemCount() {
        return shopsList.size();
    }

    @Override
    public void onSaveButtonClick(final DialogInterface dialogInterface, String userName, String password) {
        //Show Login Dialog
        final AlertDialog loading = new SpotsDialog.Builder().setContext(mContext)
                .setCancelable(false)
                .build();

        loading.show();

        FirebaseFirestore.getInstance().collection("BarberShops")
                .document(Common.city_name)
                .collection("Branch")
                .document(Common.selectedShop.getShopId())
                .collection("Barbers")
                .whereEqualTo("username",userName)
                .whereEqualTo("password",password)
                .limit(1)
                .get()
                .addOnFailureListener(e -> Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        if (task.getResult().size() > 0)
                        {
                            dialogInterface.dismiss();

                            loading.dismiss();

                            rememberListener.onUserLoginSuccess(userName);

                            //Create Barber
                            Barber barber = new Barber();
                            for (DocumentSnapshot barberSnap: task.getResult())
                            {
                                barber = barberSnap.toObject(Barber.class);
                                barber.setBarberId(barberSnap.getId());
                            }

                            barberListener.onGetBarberSuccess(barber);

                            //now we navigate to the staff home page and clear all previous activity
                            Intent staffHome = new Intent(mContext, HomeActivity.class);
                            staffHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            staffHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(staffHome);
                        }
                        else
                        {
                            loading.dismiss();
                            Toast.makeText(mContext, "Wrong Username / Password or Wrong Shop", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onCancelButtonClick(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
    }

    public class BarbershopViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_shop_name,txt_shop_address;
        CardView barber_card;

        IItemSelectedListener iItemSelectedListener;

        public void setiItemSelectedListener(IItemSelectedListener iItemSelectedListener) {
            this.iItemSelectedListener = iItemSelectedListener;
        }

        public BarbershopViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_shop_name = itemView.findViewById(R.id.shop_name);
            txt_shop_address = itemView.findViewById(R.id.shop_address);
            barber_card = itemView.findViewById(R.id.barbershop_card);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            iItemSelectedListener.onItemSelectedListener(v,getAdapterPosition());
        }
    }
}
