package com.codekid.barberstaffapp.Interfaces;

public interface IBottomSheetDialogOnDismissListener {
    void onDismissBottomSheetDialog(boolean fromButton);  //when we click confirm the dialog will be dismissed
}
