package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.ShoppingItem;

public interface IOnShoppingItemSelected {
    void onShoppingItemSelected(ShoppingItem shoppingItem);
}
