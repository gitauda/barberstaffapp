package com.codekid.barberstaffapp.Fragments;


import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codekid.barberstaffapp.Adapters.ConfirmShoppingAdapter;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Interfaces.IBottomSheetDialogOnDismissListener;
import com.codekid.barberstaffapp.Models.BarberServices;
import com.codekid.barberstaffapp.Models.FCMResponse;
import com.codekid.barberstaffapp.Models.FCMSendData;
import com.codekid.barberstaffapp.Models.Invoice;
import com.codekid.barberstaffapp.Models.MyToken;
import com.codekid.barberstaffapp.Models.ShoppingItem;
import com.codekid.barberstaffapp.R;
import com.codekid.barberstaffapp.Retrofit.IFCMService;
import com.codekid.barberstaffapp.Retrofit.RetrofitClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class TotalPriceFragment extends BottomSheetDialogFragment {

    Unbinder unbinder;

    @BindView(R.id.chip_group_services)
    ChipGroup chip_services;
    @BindView(R.id.shopping_recycler)
    RecyclerView shopping_recycler;
    @BindView(R.id.text_shop_name)
    TextView shop_name;
    @BindView(R.id.text_barber_name)
    TextView barber_name;
    @BindView(R.id.text_customer_name)
    TextView customer_name;
    @BindView(R.id.text_customer_phone)
    TextView customer_phone;
    @BindView(R.id.text_time)
    TextView txt_time;
    @BindView(R.id.text_total_price)
    TextView txt_price;
    @BindView(R.id.btn_confirm)
    Button btn_confirm;

    HashSet<BarberServices> services;
    List<ShoppingItem> shoppingItems;

    IFCMService ifcmService;
    IBottomSheetDialogOnDismissListener dismissListener;

    AlertDialog dialog;

    String image_url;

    private static TotalPriceFragment instance;

    public TotalPriceFragment(IBottomSheetDialogOnDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    public static TotalPriceFragment getInstance(IBottomSheetDialogOnDismissListener dismissListener) {
        return instance == null ? new TotalPriceFragment(dismissListener) : instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View itemView = inflater.inflate(R.layout.fragment_total_price,container,false);

       unbinder = ButterKnife.bind(this,itemView);

       init();

       initView();
       
       getBundle(getArguments());

       setInformation();

        return itemView;
    }

    private void setInformation() {
        shop_name.setText(Common.selectedShop.getName());
        barber_name.setText(Common.currentBarber.getName());
        txt_time.setText(Common.convertTimeSlotToString(Common.currentBookingInfo.getSlot().intValue()));
        customer_name.setText(Common.currentBookingInfo.getCustomerName());
        customer_phone.setText(Common.currentBookingInfo.getCustomerPhone());


        if (services.size() > 0)
        {
            // Add to chip group
            int i = 0;
            for (BarberServices servicesAdded: services)
            {
                Chip chip = (Chip)getLayoutInflater().inflate(R.layout.chip_item,null);
                chip.setText(servicesAdded.getName());
                chip.setTag(i);
                chip.setOnCloseIconClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        services.remove((int)v.getTag());
                        chip_services.removeView(v);
                        
                        calculatePrice();
                    }
                });

                chip_services.addView(chip);

                i++;
            }
        }

        if (shoppingItems.size() > 0)
        {
            ConfirmShoppingAdapter adapter = new ConfirmShoppingAdapter(getContext(),shoppingItems);
            shopping_recycler.setAdapter(adapter);
        }

        calculatePrice();
    }

    private double calculatePrice() {
        double price = Common.DEFAULT_PRICE;
        for (BarberServices servicesAdded: services)
            price += servicesAdded.getPrice();
        for (ShoppingItem item: shoppingItems)
            price += item.getPrice();

        txt_price.setText(new StringBuilder(Common.MONEY_SIGN).append(price));

        return price;
    }

    private void getBundle(Bundle arguments) {
        this.services = new Gson().fromJson(arguments.getString(Common.SERVICES_ADDED),
                new TypeToken<HashSet<BarberServices>>(){}.getType());

        this.shoppingItems = new Gson().fromJson(arguments.getString(Common.ITEMS_ADDED),
                new TypeToken<List<ShoppingItem>>(){}.getType());

        image_url = arguments.getString(Common.IMAGE_DOWNLOADABLE_URL);

    }

    private void init() {
        dialog = new SpotsDialog.Builder().setContext(getContext())
                .setCancelable(false).build();
        ifcmService = RetrofitClient.getRetrofit().create(IFCMService.class);
    }

    private void initView() {
        shopping_recycler.setHasFixedSize(true);
        shopping_recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));


        btn_confirm.setOnClickListener(v -> {
            dialog.show();

            //update booking information, set done = true
            DocumentReference bookingSet = FirebaseFirestore.getInstance()
                    .collection("BarberShops")
                    .document(Common.city_name)
                    .collection("Branch")
                    .document(Common.selectedShop.getShopId())
                    .collection("Barbers")
                    .document(Common.currentBarber.getBarberId())
                    .collection(Common.dateFormat.format(Common.bookingDate.getTime()))
                    .document(Common.currentBookingInfo.getBookingId());


            bookingSet.get()
                    .addOnCompleteListener(task -> {
                            if (task.isSuccessful())
                            {
                                if (task.getResult().exists())
                                {
                                    //update
                                    Map<String,Object> dataUpdate = new HashMap<>();
                                    dataUpdate.put("done",true);
                                    bookingSet.update(dataUpdate)
                                            .addOnFailureListener(e -> {
                                                dialog.dismiss();
                                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                            })
                                            .addOnCompleteListener(task1 -> {
                                                if (task1.isSuccessful())
                                                {
                                                    //if update is done, create invoice
                                                    createInvoice();
                                                }
                                            });
                                }
                            }
                    })
                    .addOnFailureListener(e -> {
                        dialog.dismiss();
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    });
        });
    }

    private void createInvoice() {
        //Create invoice
        CollectionReference invoiceRef = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city_name)
                .collection("Branch")
                .document(Common.selectedShop.getShopId())
                .collection("Invoices");

        Invoice invoice = new Invoice();

        invoice.setBarberId(Common.currentBarber.getBarberId());
        invoice.setBarberId(Common.currentBarber.getName());

        invoice.setBarberId(Common.selectedShop.getShopId());
        invoice.setBarberId(Common.selectedShop.getName());
        invoice.setBarberId(Common.selectedShop.getAddress());

        invoice.setBarberId(Common.currentBookingInfo.getCustomerName());
        invoice.setBarberId(Common.currentBookingInfo.getCustomerPhone());

        invoice.setImageUrl(image_url); //receive through bumdle

        invoice.setBarberServices(new ArrayList<BarberServices>(services));
        invoice.setShoppingItems(shoppingItems);

        invoice.setFinalPrice(calculatePrice());


        invoiceRef.document()
                .set(invoice)
                .addOnFailureListener(e -> Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        sendNotificationUpdateToUser(Common.currentBookingInfo.getCustomerPhone());
                    }
                });


    }

    private void sendNotificationUpdateToUser(String customerPhone) {
        //Get Token to user
        FirebaseFirestore.getInstance()
                .collection("Tokens")
                .whereEqualTo("userPhone",customerPhone)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult().size() > 0)
                    {
                        MyToken myToken = new MyToken();
                        for (DocumentSnapshot snapshot: task.getResult())
                            myToken = snapshot.toObject(MyToken.class);


                        //create Notification to send
                        FCMSendData sendData = new FCMSendData();
                        Map<String,String> dataSend = new HashMap<>();
                        dataSend.put("update_done","true");

                        //Information we need to rate barber
                        dataSend.put(Common.RATING_CITY_KEY,Common.city_name);
                        dataSend.put(Common.RATING_SHOP_ID,Common.selectedShop.getShopId());
                        dataSend.put(Common.RATING_SHOP_NAME,Common.selectedShop.getName());
                        dataSend.put(Common.RATING_BARBER_ID,Common.currentBarber.getBarberId());


                        sendData.setTo(myToken.getToken());
                        sendData.setData(dataSend);

                        ifcmService.sendNotification(sendData)
                                .subscribeOn(Schedulers.io())
                                .observeOn(Schedulers.newThread())
                                .subscribe(new Consumer<FCMResponse>() {
                                    @Override
                                    public void accept(FCMResponse fcmResponse) throws Exception {
                                        dialog.dismiss();
                                        dismiss();
                                        dismissListener.onDismissBottomSheetDialog(true);

                                    }
                                }, new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                    }
                });
    }
}
