package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.BookingInformation;

import java.util.List;

public interface ITimeSlotLoadListener {
    void onTimeSlotLoadSuccess(List<BookingInformation> timeSlots);
    void onTimeSlotLoadFailure(String message);
    void onTimeSlotLoadEmpty();
}
