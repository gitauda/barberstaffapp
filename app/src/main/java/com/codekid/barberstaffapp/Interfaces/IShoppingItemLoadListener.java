package com.codekid.barberstaffapp.Interfaces;



import com.codekid.barberstaffapp.Models.ShoppingItem;

import java.util.List;

public interface IShoppingItemLoadListener {
    void onShoppingItemLoadSuccess(List<ShoppingItem> shoppingItems);
    void onShoppingItemLoadFailure(String message);
}
