package com.codekid.barberstaffapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codekid.barberstaffapp.Adapters.ShopAdapter;
import com.codekid.barberstaffapp.Adapters.ShoppingAdapter;
import com.codekid.barberstaffapp.AfterServiceActivity;
import com.codekid.barberstaffapp.Common.SpaceItemDecoration;
import com.codekid.barberstaffapp.Interfaces.IOnShoppingItemSelected;
import com.codekid.barberstaffapp.Interfaces.IShoppingItemLoadListener;
import com.codekid.barberstaffapp.Models.ShoppingItem;
import com.codekid.barberstaffapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ShoppingFragment extends BottomSheetDialogFragment implements IShoppingItemLoadListener, IOnShoppingItemSelected {


    IOnShoppingItemSelected shoppingItemSelected;

    CollectionReference shoppingItemRef;
    IShoppingItemLoadListener shoppingItemLoadListener;

    Unbinder unbinder;

    @BindView(R.id.chip_group)
    ChipGroup chipGroup;
    @BindView(R.id.chip_wax)
    Chip chip_wax;
    @OnClick(R.id.chip_wax)
    void waxLoadClick(){
        setSelectedChip(chip_wax);
        loadShoppingItem("Wax");
    }
    @BindView(R.id.chip_spray)
    Chip chip_spray;

    @OnClick(R.id.chip_wax)
    void sprayLoadClick(){
        setSelectedChip(chip_spray);
        loadShoppingItem("Spray");
    }
    @BindView(R.id.chip_hair_care)
    Chip chip_hair_care;
    @OnClick(R.id.chip_hair_care)
    void hairCareLoadClick(){
        setSelectedChip(chip_hair_care);
        loadShoppingItem("Hair Care");
    }

    @BindView(R.id.chip_body_care)
    Chip chip_body_care;
    @OnClick(R.id.chip_body_care)
    void bodyCareLoadClick(){
        setSelectedChip(chip_body_care);
        loadShoppingItem("Body Care");
    }

    @BindView(R.id.items_recycler)
    RecyclerView recycler_item;


    private static ShoppingFragment instance;

    public static ShoppingFragment getInstance(IOnShoppingItemSelected itemSelected)
    {
        return  instance == null ? new ShoppingFragment(itemSelected) : instance;
    }

    private void loadShoppingItem(String menuItem) {
        shoppingItemRef = FirebaseFirestore.getInstance()
                .collection("Shopping")
                .document(menuItem)
                .collection("items");

        //Get Data
        shoppingItemRef.get()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        shoppingItemLoadListener.onShoppingItemLoadFailure(e.getMessage());
                    }
                }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful())
                {
                    List<ShoppingItem> shoppingItems = new ArrayList<>();
                    for (DocumentSnapshot snapshot: task.getResult())
                    {
                        ShoppingItem shoppingItem = snapshot.toObject(ShoppingItem.class);
                        shoppingItem.setId(snapshot.getId()); //add this to get product Id
                        shoppingItems.add(shoppingItem);
                    }
                    shoppingItemLoadListener.onShoppingItemLoadSuccess(shoppingItems);
                }
            }
        });
    }

    private void setSelectedChip(Chip chip_wax) {
        //Set Color
        for (int i=0;i<chipGroup.getChildCount();i++)
        {
            Chip chipItem = (Chip) chipGroup.getChildAt(i);
            if (chipItem.getId() != chip_wax.getId()) // if not Selected
            {
                chipItem.setChipBackgroundColorResource(android.R.color.darker_gray);
                chipItem.setTextColor(getResources().getColor(android.R.color.white));
            }
            else // if Selected
            {
                chipItem.setChipBackgroundColorResource(android.R.color.holo_orange_dark);
                chipItem.setTextColor(getResources().getColor(android.R.color.black));

            }
        }
    }

    public ShoppingFragment(IOnShoppingItemSelected shoppingItemSelected) {
        this.shoppingItemSelected = shoppingItemSelected;
    }

    public ShoppingFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_shopping, container, false);

        unbinder = ButterKnife.bind(this,itemView);

        shoppingItemLoadListener = this;

        //Default Load
        loadShoppingItem("Wax");

        initView();

        return  itemView;
    }

    private void initView() {
        recycler_item.setHasFixedSize(true);
        recycler_item.setLayoutManager(new GridLayoutManager(getContext(),2));
        recycler_item.addItemDecoration(new SpaceItemDecoration(8));
    }

    @Override
    public void onShoppingItemLoadSuccess(List<ShoppingItem> shoppingItems) {
        ShoppingAdapter adapter = new ShoppingAdapter(getContext(),shoppingItems,this);
        recycler_item.setAdapter(adapter);
    }

    @Override
    public void onShoppingItemLoadFailure(String message) {

    }

    @Override
    public void onShoppingItemSelected(ShoppingItem shoppingItem) {
        shoppingItemSelected.onShoppingItemSelected(shoppingItem);

    }
}

