package com.codekid.barberstaffapp;

import android.app.AlertDialog;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.TextView;
import android.widget.Toast;

import com.codekid.barberstaffapp.Adapters.ShopAdapter;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Common.SpaceItemDecoration;
import com.codekid.barberstaffapp.Interfaces.IBranchLoadListener;
import com.codekid.barberstaffapp.Interfaces.IGetBarberListener;
import com.codekid.barberstaffapp.Interfaces.IonLoadCountSalon;
import com.codekid.barberstaffapp.Interfaces.UserLoginRememberListener;
import com.codekid.barberstaffapp.Models.Barber;
import com.codekid.barberstaffapp.Models.BarberShops;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;

public class BarberShopListActivity extends AppCompatActivity implements IonLoadCountSalon, IBranchLoadListener, IGetBarberListener, UserLoginRememberListener {

    @BindView(R.id.txt_shop_count)
    TextView txt_shop_count;

    @BindView(R.id.shops_recycler)
    RecyclerView shop_recycler;

    IonLoadCountSalon ionLoadCountSalon;
    IBranchLoadListener branchLoadListener;

    ShopAdapter  adapter;

    AlertDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barber_shop_list);
        ButterKnife.bind(this);

        initView();

        init();

        loadBarberShopsBasedOnCity(Common.city_name);
    }

    private void loadBarberShopsBasedOnCity(String name) {
        dialog.show();

        ///BarberShops/Nairobi/Branch
        FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(name)
                .collection("Branch")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        List<BarberShops> shopsList = new ArrayList<>();
                        ionLoadCountSalon.onLoadShopCountSuccess(task.getResult().size());
                        for (DocumentSnapshot snapshot: task.getResult())
                        {

                            BarberShops shops = snapshot.toObject(BarberShops.class);
                            //assert shops != null;
                            shops.setShopId(snapshot.getId());
                            shopsList.add(shops);
                           // Log.i("List",shopsList.toString());
                        }
                        branchLoadListener.onAllBranchLoadSuccess(shopsList);
                    }
                }).addOnFailureListener(e -> branchLoadListener.onAllBranchLoadFailed(e.getMessage()));
    }

    private void init() {
        dialog = new SpotsDialog.Builder()
                .setContext(this)
                .setCancelable(false)
                .build();
        ionLoadCountSalon= this;
        branchLoadListener= this;
    }

    private void initView() {
        shop_recycler.setHasFixedSize(true);
        shop_recycler.setLayoutManager(new GridLayoutManager(this,2));
        shop_recycler.addItemDecoration(new SpaceItemDecoration(4));
    }

    @Override
    public void onLoadShopCountSuccess(int count) {
        txt_shop_count.setText(new StringBuilder("All Shops (").append(count).append(")"));
    }

    @Override
    public void onAllBranchLoadSuccess(List<BarberShops> shopsList) {
        adapter = new ShopAdapter(this,shopsList,this,this);
        shop_recycler.setAdapter(adapter);
        dialog.dismiss();
    }

    @Override
    public void onAllBranchLoadFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    @Override
    public void onGetBarberSuccess(Barber barber) {
        Common.currentBarber = barber;
        Paper.book().write(Common.BARBER_KEY,new Gson().toJson(barber));
    }

    @Override
    public void onUserLoginSuccess(String user) {
        //Save User
        Paper.init(this);
        Paper.book().write(Common.LOGGED_KEY,user);
        Paper.book().write(Common.CITY_NAME,Common.city_name);
        Paper.book().write(Common.SHOP_KEY,new Gson().toJson(Common.selectedShop));
    }
}
