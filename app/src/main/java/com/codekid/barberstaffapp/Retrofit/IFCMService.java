package com.codekid.barberstaffapp.Retrofit;



import com.codekid.barberstaffapp.Models.FCMResponse;
import com.codekid.barberstaffapp.Models.FCMSendData;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAdncxshA:APA91bEqv5HfZtZYX31FnzR2kowQ4CswaDPKxN7if8jsa7qJDsRQPNhJoFBiOb-82b-XobW4HsBEYsi6QXSBYPUL7cxZFTD8NZiS-RpuUxen9b8-E0cAqMVKq4shreXNtAU1kiXEtsoA"
    })
    @POST("fcm/send")
    Observable<FCMResponse> sendNotification(@Body FCMSendData body);
}
