package com.codekid.barberstaffapp.Interfaces;

import android.content.DialogInterface;

public interface IDialogClickListener {
    void onSaveButtonClick(DialogInterface dialogInterface, String userName,String password);
    void onCancelButtonClick(DialogInterface dialogInterface);
}
