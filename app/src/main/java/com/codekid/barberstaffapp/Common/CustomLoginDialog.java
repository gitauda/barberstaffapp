package com.codekid.barberstaffapp.Common;

import android.app.Dialog;
import android.content.Context;
import com.google.android.material.textfield.TextInputEditText;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.codekid.barberstaffapp.Interfaces.IDialogClickListener;
import com.codekid.barberstaffapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomLoginDialog {

    @BindView(R.id.txt_title)
    TextView txt_title;
    @BindView(R.id.edt_user)
    TextInputEditText edt_user;
    @BindView(R.id.edt_password)
    TextInputEditText edt_pwd;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.btn_cancel)
    Button btn_cancel;


    public static CustomLoginDialog mDialog;
    public IDialogClickListener iDialogClickListener;


    public static CustomLoginDialog getInstance() {
        if (mDialog == null)
            mDialog = new CustomLoginDialog();

        return mDialog;
    }

    public void showLoginDialog(String title, String saveText, String cancelText,
                                Context context,
                                final IDialogClickListener dialogClickListener)
    {
        this.iDialogClickListener = dialogClickListener;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.login_layout);

        ButterKnife.bind(this,dialog);
        //Set Title
        if (!TextUtils.isEmpty(title))
        {
            txt_title.setText(title);
            txt_title.setVisibility(View.VISIBLE);
        }

        btn_login.setText(saveText);
        btn_cancel.setText(cancelText);

        dialog.setCancelable(false);
        dialog.show();

        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogClickListener.onSaveButtonClick(dialog,edt_user.getText().toString(),edt_pwd.getText().toString());
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogClickListener.onCancelButtonClick(dialog);
            }
        });
    }
}
