package com.codekid.barberstaffapp.Interfaces;

public interface INotificationCountListener {
    void onNotificationCountSuccess(int count);
}
