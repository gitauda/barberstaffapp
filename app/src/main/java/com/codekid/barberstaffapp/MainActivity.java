package com.codekid.barberstaffapp;

import android.Manifest;
import android.app.AlertDialog;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.codekid.barberstaffapp.Adapters.BranchAdapter;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Common.SpaceItemDecoration;
import com.codekid.barberstaffapp.Interfaces.ICityLoadListener;
import com.codekid.barberstaffapp.Models.Barber;
import com.codekid.barberstaffapp.Models.BarberShops;
import com.codekid.barberstaffapp.Models.City;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity implements ICityLoadListener {

    @BindView(R.id.city_recycler)
    RecyclerView branch_recycler;

    CollectionReference allBarberShopsCollection;

    ICityLoadListener iBranchLoadListener;

    BranchAdapter adapter;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                FirebaseInstanceId.getInstance()
                        .getInstanceId()
                        .addOnFailureListener(e -> Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show())
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful())
                            {
                                Common.updateToken(MainActivity.this,
                                        task.getResult().getToken());
                                Log.d("TOKEN",task.getResult().getToken());
                            }
                        });

                Paper.init(MainActivity.this);
                String user = Paper.book().read(Common.LOGGED_KEY);
                if (TextUtils.isEmpty(user)) //if user wasn't logged in before
                {
                    ButterKnife.bind(MainActivity.this);

                    initView();

                    init();

                    loadBranchesFromFirestore();

                }
                else //if the user is already logged in
                {
                    //Auto Login
                    Gson gson = new Gson();
                    Common.city_name = Paper.book().read(Common.CITY_NAME);
                    Common.selectedShop = gson.fromJson(Paper.book().read(Common.SHOP_KEY,""),new TypeToken<BarberShops>(){}.getType());
                    Common.currentBarber = gson.fromJson(Paper.book().read(Common.BARBER_KEY,""),new TypeToken<Barber>(){}.getType());

                    Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();






    }

    private void loadBranchesFromFirestore() {
        dialog.show();

        allBarberShopsCollection.get()
                .addOnFailureListener(e -> iBranchLoadListener.onAllBranchLoadFailed(e.getMessage()))
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        List<City> cities = new ArrayList<>();
                        for (DocumentSnapshot citySnap: task.getResult())
                        {
                            City city = citySnap.toObject(City.class);
                            cities.add(city);
                        }
                        iBranchLoadListener.onAllBranchLoadSuccess(cities);
                    }
                });
    }

    private void init() {
        allBarberShopsCollection = FirebaseFirestore.getInstance().collection("BarberShops");
        iBranchLoadListener = this;
        dialog = new SpotsDialog.Builder().setCancelable(false).setContext(this).build();
    }

    private void initView() {
        branch_recycler.setHasFixedSize(true);
        branch_recycler.setLayoutManager(new GridLayoutManager(this,2));
        branch_recycler.addItemDecoration(new SpaceItemDecoration(8));
    }

    @Override
    public void onAllBranchLoadSuccess(List<City> cityList) {
        adapter = new BranchAdapter(this,cityList);
        branch_recycler.setAdapter(adapter);

        dialog.dismiss();
    }

    @Override
    public void onAllBranchLoadFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
