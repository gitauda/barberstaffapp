package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.BarberShops;


import java.util.ArrayList;
import java.util.List;

public interface IBranchLoadListener {
    void onAllBranchLoadSuccess(List<BarberShops> shopsList);
    void onAllBranchLoadFailed(String message);
}
