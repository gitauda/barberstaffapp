package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.Barber;

public interface IGetBarberListener {
    void onGetBarberSuccess(Barber barber);
}
