package com.codekid.barberstaffapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.codekid.barberstaffapp.Common.DiffCallBack;
import com.codekid.barberstaffapp.Models.MyNotification;
import com.codekid.barberstaffapp.R;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {


    Context context;
    List<MyNotification> notificationList;

    public NotificationAdapter(Context context, List<MyNotification> notificationList) {
        this.context = context;
        this.notificationList = notificationList;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_notification_item,viewGroup,false);

        return new NotificationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder notificationViewHolder, int i) {
        notificationViewHolder.notification_title.setText(notificationList.get(i).getTitle());
        notificationViewHolder.notification_content.setText(notificationList.get(i).getContent());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public void updateList(List<MyNotification> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffCallBack(this.notificationList,newList));
        notificationList.addAll(newList);
        diffResult.dispatchUpdatesTo(this);
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_noti_title)
        TextView notification_title;
        @BindView(R.id.txt_noti_content)
        TextView notification_content;



        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
