package com.codekid.barberstaffapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Fragments.ShoppingFragment;
import com.codekid.barberstaffapp.Fragments.TotalPriceFragment;
import com.codekid.barberstaffapp.Interfaces.IBarberServicesLoadListener;
import com.codekid.barberstaffapp.Interfaces.IBottomSheetDialogOnDismissListener;
import com.codekid.barberstaffapp.Interfaces.IOnShoppingItemSelected;
import com.codekid.barberstaffapp.Models.BarberServices;
import com.codekid.barberstaffapp.Models.ShoppingItem;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

public class AfterServiceActivity extends AppCompatActivity implements IBarberServicesLoadListener, IOnShoppingItemSelected, IBottomSheetDialogOnDismissListener {


    private static final int MY_CAMERA_REQUEST_CODE = 5555;
    @BindView(R.id.txt_customer_name)
    TextView txt_customer_name;

    @BindView(R.id.txt_customer_phone)
    TextView txt_customer_phone;

    @BindView(R.id.chip_services)
    ChipGroup services_chip;

    @BindView(R.id.shopping_chip)
    ChipGroup shopping_chip;

    @BindView(R.id.edt_services)
    AppCompatAutoCompleteTextView edt_services;

    @BindView(R.id.customer_img)
    ImageView customer_img;

    @BindView(R.id.btn_finish)
    Button btn_finish;

    @BindView(R.id.add_shopping)
    ImageView add_shopping;

    @BindView(R.id.radio_no_pic)
    RadioButton radio_no_pic;
    @BindView(R.id.radio_pic)
    RadioButton radio_pic;

    AlertDialog dialog;

    IBarberServicesLoadListener servicesLoadListener;

    HashSet<BarberServices> serviceAdded = new HashSet<>();

    LayoutInflater inflater;

    List<ShoppingItem> shoppingItems = new ArrayList<>();

    Uri fileUri;

    StorageReference storageReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_service);
        ButterKnife.bind(this);

        setCustomerInformation();
        
        loadBarberServices();

        init();

        initView();
    }

    private void initView() {

        getSupportActionBar().setTitle("Check Out");


        customer_img.setOnClickListener(view -> {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            fileUri = getOutputMediaFileUri();
            intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
            startActivityForResult(intent,MY_CAMERA_REQUEST_CODE);
        });

        add_shopping.setOnClickListener(v -> {
            ShoppingFragment fragment = new ShoppingFragment(AfterServiceActivity.this);
            fragment.show(getSupportFragmentManager(),"Shopping");
        });

        btn_finish.setOnClickListener(v ->{
            if (radio_no_pic.isChecked())
            {
                dialog.dismiss();

                TotalPriceFragment fragment = TotalPriceFragment.getInstance(AfterServiceActivity.this);
                Bundle bundle = new Bundle();
                bundle.putString(Common.SERVICES_ADDED,new Gson().toJson(serviceAdded));
                bundle.putString(Common.ITEMS_ADDED,new Gson().toJson(shoppingItems));
                fragment.setArguments(bundle);
                fragment.show(getSupportFragmentManager(),"Price");

            }else {
                uploadImage(fileUri);
            }

        });


        radio_pic.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
            {
                customer_img.setVisibility(View.VISIBLE);
                btn_finish.setEnabled(false);
            }
        });

        radio_no_pic.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
            {
                customer_img.setVisibility(View.GONE);
                btn_finish.setEnabled(true);
            }
        });


    }

    private void uploadImage(Uri fileUri) {
        if (fileUri != null)
        {
            dialog.show();

            String file  = Common.getFileName(getContentResolver(),fileUri);
            String path = new StringBuilder("Customer_Pictures/")
                    .append(file)
                    .toString();

            storageReference = FirebaseStorage.getInstance()
                    .getReference(path);

            UploadTask uploadTask = storageReference.putFile(fileUri);

            //Create task
            Task<Uri> task = uploadTask.continueWithTask(task12 -> {
               if (!task12.isSuccessful())
                   Toast.makeText(AfterServiceActivity.this, "Failed to Upload", Toast.LENGTH_SHORT).show();

                   return storageReference.getDownloadUrl();

            }).addOnCompleteListener(task1 -> {
                if (task1.isSuccessful())
                {
                    String url = task1.getResult().toString().substring(0, task1.getResult().toString().indexOf("&token"));
                    Log.d("DOWNLOADABLE_LINK",url);

                    //generate final billing here

                    dialog.dismiss();


                    TotalPriceFragment fragment = TotalPriceFragment.getInstance(AfterServiceActivity.this);
                    Bundle bundle = new Bundle();
                    bundle.putString(Common.SERVICES_ADDED,new Gson().toJson(serviceAdded));
                    bundle.putString(Common.ITEMS_ADDED,new Gson().toJson(shoppingItems));
                    bundle.putString(Common.IMAGE_DOWNLOADABLE_URL,url);
                    fragment.setArguments(bundle);
                    fragment.show(getSupportFragmentManager(),"Price");
                }
            }).addOnFailureListener(e -> {
                dialog.dismiss();
                Toast.makeText(AfterServiceActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            });

        }

    }

    private Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }


    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"BARBERSHOP PICS");
        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdir())
                return null;
        }

        String time_stamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath()+File.separator+"IMG"+time_stamp+"_"+new Random().nextInt()+"jpg");

        return mediaFile;
    }

    private void init() {
        dialog = new SpotsDialog.Builder().setContext(this)
                .setCancelable(false)
                .build();

        inflater = LayoutInflater.from(this);

        servicesLoadListener = this;
    }

    private void loadBarberServices() {

//       dialog.show();

        FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city_name)
                .collection("Branch")
                .document(Common.selectedShop.getShopId())
                .collection("Services")
                .get()
                .addOnFailureListener(e -> servicesLoadListener.onBarberServicesLoadFailed(e.getMessage()))
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful())
                    {
                        List<BarberServices> barberServices = new ArrayList<>();

                        for (DocumentSnapshot serviceSnap : task.getResult())
                        {
                            BarberServices services = serviceSnap.toObject(BarberServices.class);
                            barberServices.add(services);
                        }
                        servicesLoadListener.onBarberServicesLoadSuccess(barberServices);
                    }
                });
    }

    private void setCustomerInformation() {
        txt_customer_name.setText(Common.currentBookingInfo.getCustomerName());
        txt_customer_phone.setText(Common.currentBookingInfo.getCustomerPhone());
    }

    @Override
    public void onBarberServicesLoadSuccess(List<BarberServices> servicesList) {
        List<String> nameServices = new ArrayList<>();
        //sort alphabetically
        Collections.sort(servicesList, (barberServices, barberserv) -> barberServices.getName().compareTo(barberserv.getName()));

        //Add the name of all services after sorting
        for (BarberServices barberServices:servicesList)
            nameServices.add(barberServices.getName());

        //create Adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.select_dialog_item,nameServices);

        edt_services.setThreshold(1); //will start working from first character
        edt_services.setAdapter(adapter);
        edt_services.setOnItemClickListener((parent, view, position, id) -> {
            //Add to chip Group
            int index = nameServices.indexOf(edt_services.getText().toString().trim());

            if (!serviceAdded.contains(servicesList.get(index))){
                serviceAdded.add(servicesList.get(index)); //to avoid duplicating services in the list we'll use a hashSet
                Chip  item = (Chip) inflater.inflate(R.layout.chip_item, null);
                item.setText(edt_services.getText().toString());
                item.setTag(index);
                edt_services.setText("");

                item.setOnCloseIconClickListener(v -> {
                    services_chip.removeView(v);
                    serviceAdded.remove((int)item.getTag());
                });

                services_chip.addView(item);
            }
            else
            {
                edt_services.setText("");
            }

        });
    }

    @Override
    public void onBarberServicesLoadFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    @Override
    public void onShoppingItemSelected(ShoppingItem shoppingItem) {
        // create a list to hold shopping items
        shoppingItems.add(shoppingItem);
        Log.d("ShoppingItem",""+shoppingItems.size());

        Chip  item = (Chip) inflater.inflate(R.layout.chip_item, null);
        item.setText(shoppingItem.getName());
        item.setTag(shoppingItems.indexOf(shoppingItem));
        edt_services.setText("");

        item.setOnCloseIconClickListener(v -> {
            shopping_chip.removeView(v);
            shoppingItems.remove((int)item.getTag());
        });

        services_chip.addView(item);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_CAMERA_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                Bitmap bitmap = null;
                ExifInterface ei = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),fileUri);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        ei = new ExifInterface(getContentResolver().openInputStream(fileUri));
                    }

                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_UNDEFINED);

                    Bitmap rotateBitmap = null;
                    switch (orientation)
                    {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotateBitmap = rotateImage(bitmap,90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotateBitmap = rotateImage(bitmap,180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotateBitmap = rotateImage(bitmap,270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                            default:
                                rotateBitmap = bitmap;
                            break;
                    }
                    customer_img.setImageBitmap(rotateBitmap);
                    btn_finish.setEnabled(true);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap rotateImage(Bitmap bitmap, int i) {
        Matrix matrix = new Matrix();
        matrix.postRotate(i);

        return Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight()
                ,matrix,true);
    }

    @Override
    public void onDismissBottomSheetDialog(boolean fromButton) {
        if (fromButton) // == true
            finish();
    }
}
