package com.codekid.barberstaffapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codekid.barberstaffapp.Adapters.NotificationAdapter;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Interfaces.INotificationLoadListener;
import com.codekid.barberstaffapp.Models.MyNotification;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends AppCompatActivity implements INotificationLoadListener {

    @BindView(R.id.notification_recycler)
    RecyclerView notification_recycler;

    CollectionReference notificationCollection;

    INotificationLoadListener notificationLoadListener;
    List<MyNotification> firstList = new ArrayList<>();


    int total_item =0, last_visible_item;
    boolean isLoading= false,isMaxData=false;
    DocumentSnapshot finalDoc;
    NotificationAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        init();

        initView();

        loadNotification(null);
    }

    private void initView() {
        notification_recycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        notification_recycler.setLayoutManager(layoutManager);
        notification_recycler.addItemDecoration(new DividerItemDecoration(this,layoutManager.getOrientation()));

        notification_recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                total_item = layoutManager.getItemCount();
                last_visible_item = layoutManager.findLastVisibleItemPosition();


                if (!isLoading && total_item <= (last_visible_item + Common.MAX_NOTIFICATION_PER_LOAD))
                {
                    loadNotification(finalDoc);
                    isLoading = true;
                }
            }
        });

    }

    private void loadNotification(DocumentSnapshot lastDoc) {
        notificationCollection = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city_name)
                .collection("Branch")
                .document(Common.selectedShop.getShopId())
                .collection("Barber")
                .document(Common.currentBarber.getBarberId())
                .collection("Notifications");

        if (lastDoc == null)
        {
            notificationCollection.orderBy("serverTimeStamp", Query.Direction.DESCENDING)
                    .limit(Common.MAX_NOTIFICATION_PER_LOAD)
                    .get()
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            notificationLoadListener.onNotificationLoadFailed(e.getMessage());
                        }
                    })
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful())
                            {
                                List<MyNotification> myNotifications = new ArrayList<>();
                                DocumentSnapshot finalDoc = null;
                                for (DocumentSnapshot snapshot : task.getResult())
                                {
                                    MyNotification myNotification = snapshot.toObject(MyNotification.class);
                                    myNotifications.add(myNotification);
                                    finalDoc = snapshot;
                                }

                                notificationLoadListener.onNotificationLoadSuccess(myNotifications,finalDoc);
                            }
                        }
                    });
        }
        else
        {
            if (!isMaxData)
            {
                notificationCollection.orderBy("serverTimeStamp", Query.Direction.DESCENDING)
                        .startAfter(lastDoc)
                        .limit(Common.MAX_NOTIFICATION_PER_LOAD)
                        .get()
                        .addOnFailureListener(e -> notificationLoadListener.onNotificationLoadFailed(e.getMessage()))
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful())
                            {
                                List<MyNotification> myNotifications = new ArrayList<>();
                                DocumentSnapshot finalDoc = null;
                                for (DocumentSnapshot snapshot : task.getResult())
                                {
                                    MyNotification myNotification = snapshot.toObject(MyNotification.class);
                                    myNotifications.add(myNotification);
                                    finalDoc = snapshot;
                                }

                                notificationLoadListener.onNotificationLoadSuccess(myNotifications,finalDoc);
                            }
                        });
            }
        }

    }

    private void init() {
        notificationLoadListener = this;
    }

    @Override
    public void onNotificationLoadSuccess(List<MyNotification> notificationList, DocumentSnapshot lastDocument) {
        if (lastDocument != null)
        {

            if (lastDocument.equals(finalDoc))
                isMaxData = true;
            else
            {
                finalDoc = lastDocument;
                isMaxData = false;
            }

            if (adapter == null && firstList.size() == 0)
            {
                adapter = new NotificationAdapter(this,notificationList);
                firstList = notificationList;
            }
            else
            {
                if (!notificationList.equals(firstList))
                    adapter.updateList(notificationList);
            }


            notification_recycler.setAdapter(adapter);
        }
    }

    @Override
    public void onNotificationLoadFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
