package com.codekid.barberstaffapp.Interfaces;

import com.codekid.barberstaffapp.Models.MyNotification;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.List;

public interface INotificationLoadListener {
    void onNotificationLoadSuccess(List<MyNotification> notificationList, DocumentSnapshot lastDocument);
    void onNotificationLoadFailed(String message);
}
