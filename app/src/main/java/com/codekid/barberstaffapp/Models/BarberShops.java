package com.codekid.barberstaffapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class BarberShops implements Parcelable  {
    private String name,address,website,phone,openHours,shopId;

    public BarberShops() {
    }

    protected BarberShops(Parcel in) {
        name = in.readString();
        address = in.readString();
        website = in.readString();
        phone = in.readString();
        openHours = in.readString();
        shopId = in.readString();
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(website);
        dest.writeString(phone);
        dest.writeString(openHours);
        dest.writeString(shopId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BarberShops> CREATOR = new Creator<BarberShops>() {
        @Override
        public BarberShops createFromParcel(Parcel in) {
            return new BarberShops(in);
        }

        @Override
        public BarberShops[] newArray(int size) {
            return new BarberShops[size];
        }
    };


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOpenHours() {
        return openHours;
    }

    public void setOpenHours(String openHours) {
        this.openHours = openHours;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }


}
