package com.codekid.barberstaffapp.Interfaces;

public interface UserLoginRememberListener {
    void onUserLoginSuccess(String user);
}
