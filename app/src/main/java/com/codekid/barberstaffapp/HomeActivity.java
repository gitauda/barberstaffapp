package com.codekid.barberstaffapp;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.codekid.barberstaffapp.Adapters.TimeSlotAdapter;
import com.codekid.barberstaffapp.Common.Common;
import com.codekid.barberstaffapp.Common.SpaceItemDecoration;
import com.codekid.barberstaffapp.Interfaces.INotificationCountListener;
import com.codekid.barberstaffapp.Models.BookingInformation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.codekid.barberstaffapp.Interfaces.ITimeSlotLoadListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;

public class HomeActivity extends AppCompatActivity implements ITimeSlotLoadListener, INotificationCountListener {



    @BindView(R.id.activity_home)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView navigationView;

    ActionBarDrawerToggle actionBarDrawerToggle;

    ITimeSlotLoadListener timeSlotLoadListener;

    /*=============COPY====================*/
    DocumentReference barberDoc;

    ITimeSlotLoadListener timeSlotListener;

    AlertDialog dialog;

    @BindView(R.id.time_slot_recycler)
    RecyclerView time_recycler;
    @BindView(R.id.calendar)
    HorizontalCalendarView calendarView;

    SimpleDateFormat dateFormat;
    /*=============END COPY====================*/

    TextView txt_not_badge;

    CollectionReference notificationCollection;
    CollectionReference currentBookDate;

    EventListener<QuerySnapshot> notificationEvent;
    EventListener<QuerySnapshot> bookingEvent;

    ListenerRegistration notificationListener;
    ListenerRegistration bookingListener;

    INotificationCountListener countListener;


    TextView txt_barber_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        init();

        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;

        if (item.getItemId() == R.id.action_new_notification)
        {
            startActivity(new Intent(HomeActivity.this,NotificationActivity.class));
            txt_not_badge.setText("");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private void initView() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            if (menuItem.getItemId() == R.id.menu_exit)
                logOut();
            return false;
        });

        View headerView = navigationView.getHeaderView(0);
        txt_barber_name = headerView.findViewById(R.id.txt_barber_name);
        txt_barber_name.setText(Common.currentBarber.getName());

        dialog = new SpotsDialog.Builder().setContext(this).setCancelable(false).build();

        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE,0); // Add Current Date
        loadAllAvailableTimeSlots(Common.currentBarber.getBarberId(), Common.dateFormat.format(date.getTime()));


        time_recycler.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);
        time_recycler.setLayoutManager(gridLayoutManager);
        time_recycler.addItemDecoration(new SpaceItemDecoration(8));


        //Calendar
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.DATE,0);
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE,2); //2 days


        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this,R.id.calendar)
                .range(startDate,endDate)
                .datesNumberOnScreen(1)
                .mode(HorizontalCalendar.Mode.DAYS)
                .defaultSelectedDate(startDate)
                .configure()
                .end()
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                if (Common.bookingDate.getTimeInMillis() != date.getTimeInMillis())
                {
                    Common.bookingDate = date; // This code will not load again if u select new Date with same date selected
                    loadAllAvailableTimeSlots(Common.currentBarber.getBarberId(),Common.dateFormat.format(date.getTime()));
                }
            }
        });
    }

    private void logOut() {
        //just delete all remember keys and revert to Main Actvity
        Paper.init(this);
        Paper.book().delete(Common.SHOP_KEY);
        Paper.book().delete(Common.BARBER_KEY);
        Paper.book().delete(Common.CITY_NAME);
        Paper.book().delete(Common.LOGGED_KEY);

        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to logout??")
                .setCancelable(false)
                .setPositiveButton("YES", (dialog, which) -> {

                    Intent mainActivity = new Intent(HomeActivity.this,MainActivity.class);
                    mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mainActivity);
                    finish();

                })
                .setNegativeButton("CANCEL", (dialog, which) -> dialog.dismiss()).show();



    }

    private void loadAllAvailableTimeSlots(String barberId,final String bookDate) {
        dialog.show();
        ///BarberShops/Nairobi/Branch/brv6UA3HkpP1H309IwGS/Barbers/eR5ZHuVOstBBDt72ZLtB



        barberDoc.get()
                .addOnCompleteListener(task -> {
                    DocumentSnapshot snapshot = task.getResult();
                    if (snapshot.exists()) //if Barber is available
                    {
                        //Get Booking info
                        //if not created return empty
                        CollectionReference date = FirebaseFirestore.getInstance()
                                .collection("BarberShops")
                                .document(Common.city_name)
                                .collection("Branch")
                                .document(Common.selectedShop.getShopId())
                                .collection("Barbers")
                                .document(Common.currentBarber.getBarberId())
                                .collection(bookDate);


                        date.get()
                                .addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful()){
                                        QuerySnapshot snap = task1.getResult();
                                        if (snap.isEmpty()) //if there is no appointment
                                            timeSlotLoadListener.onTimeSlotLoadEmpty();
                                        else
                                        {
                                            List<BookingInformation> timeSlotList =  new ArrayList<>();

                                            for (QueryDocumentSnapshot document : task1.getResult()){

                                                timeSlotList.add(document.toObject(BookingInformation.class));
                                            }
                                            timeSlotLoadListener.onTimeSlotLoadSuccess(timeSlotList);
                                        }
                                    }
                                });

                    }

                })
                .addOnFailureListener(e -> {
            timeSlotLoadListener.onTimeSlotLoadFailure(e.getMessage());
        });


    }

    private void init() {
        timeSlotLoadListener = this;
        countListener = this;
        initNotificationRealTimeUpdate();
        initBookingRealtimeUpdate();
    }

    private void initBookingRealtimeUpdate() {
        barberDoc = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city_name)
                .collection("Branch")
                .document(Common.selectedShop.getShopId())
                .collection("Barbers")
                .document(Common.currentBarber.getBarberId());


        //Get Current date
        Calendar date = Calendar.getInstance();
        date.add(Calendar.DATE,0);
        bookingEvent = (queryDocumentSnapshots, e) -> {

            //if have any new booking, update adapter
            loadAllAvailableTimeSlots(Common.currentBarber.getBarberId(),Common.dateFormat.format(date.getTime()));

            currentBookDate = barberDoc.collection(Common.dateFormat.format(date.getTime()));

            bookingListener = currentBookDate.addSnapshotListener(bookingEvent);
        };
    }

    private void initNotificationRealTimeUpdate() {
        notificationCollection = FirebaseFirestore.getInstance()
                .collection("BarberShops")
                .document(Common.city_name)
                .collection("Branch")
                .document(Common.selectedShop.getShopId())
                .collection("Barber")
                .document(Common.currentBarber.getBarberId())
                .collection("Notifications");


        notificationEvent = (queryDocumentSnapshots, e) -> {
            if (queryDocumentSnapshots.size() > 0)
                loadNotifications();
        };

        notificationListener = notificationCollection.whereEqualTo("read",false) //only listen and count notifications that are unread
                .addSnapshotListener(notificationEvent);
    }


    public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit ?")
                .setCancelable(false)
                .setPositiveButton("YES", (dialog, which) -> Toast.makeText(HomeActivity.this,"Test Exit",Toast.LENGTH_SHORT).show())
                .setNegativeButton("CANCEL", (dialog, which) -> {
                    dialog.dismiss();
                });
    }

    @Override
    public void onTimeSlotLoadSuccess(List<BookingInformation> timeSlots) {
        TimeSlotAdapter adapter = new TimeSlotAdapter(this,timeSlots);
        time_recycler.setAdapter(adapter);

        dialog.dismiss();
    }

    @Override
    public void onTimeSlotLoadFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

    @Override
    public void onTimeSlotLoadEmpty() {
        TimeSlotAdapter adapter = new TimeSlotAdapter(this);
        time_recycler.setAdapter(adapter);

        dialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.staff_home_menu,menu);
        MenuItem menuItem = menu.findItem(R.id.action_new_notification);

        txt_not_badge = menuItem.getActionView().findViewById(R.id.notification_badge);

        loadNotifications();

        menuItem.getActionView().setOnClickListener(v -> onOptionsItemSelected(menuItem));

        return super.onCreateOptionsMenu(menu);
    }

    private void loadNotifications() {
        notificationCollection.whereEqualTo("read",false)
                .get()
                .addOnFailureListener(e -> {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }).addOnCompleteListener(task -> {
            if (task.isSuccessful())
            {
                countListener.onNotificationCountSuccess(task.getResult().size());
            }
        });
    }

    @Override
    public void onNotificationCountSuccess(int count) {
        if (count == 0)
            txt_not_badge.setVisibility(View.INVISIBLE);
        else
        {
            txt_not_badge.setVisibility(View.VISIBLE);
            if (count <= 9)
                txt_not_badge.setText(String.valueOf(count));
            else
                txt_not_badge.setText("9+");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initBookingRealtimeUpdate();
        initNotificationRealTimeUpdate();
    }

    @Override
    protected void onStop() {
        if (notificationListener != null)
            notificationListener.remove();
        if (bookingListener != null)
            bookingListener.remove();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (notificationListener != null)
            notificationListener.remove();
        if (bookingListener != null)
            bookingListener.remove();
        super.onDestroy();
    }
}
